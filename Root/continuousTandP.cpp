/*!
  \file continuousTandP.cpp
  \brief Perform the pseudo-continuous calibration from inclusive calibration at various WP
*/

#include <string>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <cmath>
#include <cstdarg> // var args

#include <TROOT.h>
#include <TSystem.h>
#include <TNamed.h>
#include <TApplication.h>
#include <TStyle.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TString.h>
#include <TLatex.h>

#include <TH1.h>
#include <TH2F.h>
#include <TH3F.h>
#include <TMath.h>
#include <TVectorD.h>
#include <TMatrixD.h>
#include <TMatrixDSym.h>
#include <TMatrixDSymEigen.h>

#include "operatingpoint.h"
#include "readfile.h"
#include "computecomp.h"
#include "syst.h"
#include "binstat.h"
#include "reduction.h"
#include "MV2cBins.h"

bool verbose = false;
bool verbose_final = true;
bool drawPlots = true;

stringstream sfilePath;
float alpha = 0.9; // fill color transparency

using namespace std;

TApplication app("app",0,NULL);

double Vii(binstat bin, double sumVkk, double vscale_i);
double Vij(binstat bin_i, binstat bin_j, double sumVkk, double vscale_i, double vscale_j = 1);
vector<double> getBins(vector<string> sbins);
void setHists(TH3F *h);
void printCanvas(TCanvas *c, string filename);

/*!
  \brief Main code performing the continuous calibration
  \param argv List of text files
*/
int main(int argc, char* argv[]){
   
  gROOT->SetBatch(kTRUE);
  gErrorIgnoreLevel = kWarning;

  //std::cout.precision(3);

  setMV2cBins();

  bool applyReduction = false;
  bool lremove_extra = false;

  int flav = -1;
  string flavour;
  string tagger;
  string jetcoll;
  vector <op> ops; // operating points information from the cumulative calibration
  vector<double> tagweight; // operating points' tag weight values

  // ----------------------------------------------------------------------
  // read input files, get operating points and calibration files
  // if user analysis efficiencies are given in a root file in addition
  // to the calibration analysis SF in the txt file, retrieve all numbers
  // ----------------------------------------------------------------------
  string input(argv[1],strlen(argv[1]));
  string rootfile = "";

  tagweight.push_back(-1.);
  for (int i=1; i<argc; i++) {
    string sfile(argv[i],strlen(argv[i]));
    ifstream ifile(sfile.c_str());

    // retrieve the file's configuration
    string str; getline(ifile,str);
    if (str.length() < 1)
    {
      cout << "Make sure the first line of your inputs file is not empty" << endl;
    }
    vector<string> sub = substrings(str,"(",",",")");
    string tagw;

    flavour = sub[1];
    tagger  = sub[2];
    tagw    = sub[3];// tag weight in run 1, efficiency point in run 2
    jetcoll = sub[4];

    map<string,map<int,double> > effmap;
    if (TString(jetcoll).Contains("TrackJets")) {
      effmap = effmapTrack;
    }
    else {
      effmap = effmapTopo;
    }

    //if(verbose)
    cout << sfile << " ---> " << "Processing  " << jetcoll << "  " << flavour << "  " << tagger << "  " << tagw << endl;

    // run 2 naming : the efficiency cut is written instead of the wp weight
    if(tagw.find("_") != string::npos) {
      int eff = atof(tagw.substr(tagw.find("_")+1).c_str());
      tagweight.push_back(effmap[tagger][eff]);
    } else
      tagweight.push_back(atof(tagw.c_str()));

    op tmp;
    readfile(sfile.c_str(),tmp);
    ops.push_back(tmp);

    if (flavour == "light") flav = 0;
    if (flavour == "charm") flav = 4;
    if (flavour == "beauty" ||
	flavour == "bottom" ) flav = 5;
    if (flavour == "tau") flav = 15;

    if((int)tmp.extratio.size() > 0)
      lremove_extra = true;

    if (flav < 0) {
      cout << "Exiting : no valid flavour retrieved" << endl;
      exit(1);
    }
  }
  tagweight.push_back(1);

  TString config;
  config += tagger;
  config += "_";
  config += jetcoll;

  vector<double> pTbins  = getBins(ops[0].pt);
  vector<double> etabins = getBins(ops[0].eta);
  const int npTbins  = pTbins.size()-1;
  const int netabins = etabins.size()-1;
  const int ntbins   = ops.size() + 1;// tag bins
  const int nkbins   = npTbins*netabins;// kinematic bins

  if (ntbins != argc) {
    cout << "Number of OPs doesn't match" << endl;
    exit(1);
  }

  // print some informations about the calibration under process
  cout << "-------------------------------------------------" << endl;
  // general informations
  cout << flavour;
  if (jetcoll.find("EM") != string::npos)
    cout << " --> EM Jets ";
  else
    cout << " --> LC Jets ";
  cout << " --> " << tagger;
  cout << endl;
  // operating points / tag weight bin boundaries
  cout << "Tagweight :  ";
  for (int i=0; i<(int)tagweight.size(); i++)
    cout << tagweight[i] << "  ";
  cout << endl;
  // kinematic bins
  cout << "[pT]";  for (int ipt=0; ipt<npTbins; ipt++)  {cout << "[" << pTbins[ipt]  << ";" << pTbins[ipt+1]  << "]";} cout << endl;
  cout << "[eta]"; for (int ieta=0; ieta<netabins; ieta++) {cout << "[" << etabins[ieta] << ";" << etabins[ieta+1] << "]";} cout << endl;
  cout << "-------------------------------------------------" << endl;

  // --------------------------------------------------------------------
  // read the user analysis efficiencies contained in the input root file
  // --------------------------------------------------------------------
  vector<vector<double> > xtrue_eff;  xtrue_eff.resize(nkbins);
  for (int kbin=0; kbin<nkbins; kbin++)
    {
      xtrue_eff[kbin].resize(ntbins,0);
    }

  // -------------------------------------------------------
  // declare and intialize all needed vectors and histograms
  // -------------------------------------------------------
  // covariance matrix
  TMatrixDSym cov_stat(npTbins*netabins*ntbins); // to be written in the output root file
  //TMatrixDSym cov_tot(npTbins*netabins*ntbins);
  // eventually used for eigen vector decomposition
  TMatrixD cov(npTbins*netabins*ntbins,npTbins*netabins*ntbins); // used for reduction
  // scale factors
  TH3F *th2_sf            = new TH3F("SF", "SF", npTbins, &pTbins[0], netabins, &etabins[0], ntbins, &tagweight[0]);
  setHists(th2_sf);
  // systematics
  TH2F hsyst("hsyst","hsyst",ntbins*npTbins*netabins,0,ntbins*npTbins*netabins,ntbins*npTbins*netabins,0,ntbins*npTbins*netabins);
  map<string, double>::iterator it;
  map<string,TH3F*> mth2_sys;
  map<string,TH3F*> mth2_usys;
  mth2_sys["MCreference"] = new TH3F("MCreference", "MCreference", npTbins, &pTbins[0], netabins, &etabins[0], ntbins, &tagweight[0]);
  mth2_sys["systematics"] = new TH3F("systematics", "systematics", npTbins, &pTbins[0], netabins, &etabins[0], ntbins, &tagweight[0]);

  for (size_t i = 0; i < ops.size(); i++) // loop in case there are uncorrelated systematics amongst WP
  {
    for (it  = ops[i].sf_sys[0].begin(); it != ops[i].sf_sys[0].end(); it++) {
      if (mth2_sys.find(it->first) == mth2_sys.end() ) 
      {
        mth2_sys[it->first] =  new TH3F((it->first).c_str(), (it->first).c_str(), npTbins, &pTbins[0], netabins, &etabins[0], ntbins, &tagweight[0]);
        setHists(mth2_sys[it->first]);
      }
    }
    for (it  = ops[i].sf_usys[0].begin(); it != ops[i].sf_usys[0].end(); it++) 
    {
      if (mth2_usys.find(it->first) == mth2_usys.end() ) 
      {
        mth2_usys[it->first] =  new TH3F((it->first).c_str(), (it->first).c_str(), npTbins,&pTbins[0], netabins, &etabins[0], ntbins, &tagweight[0]);
        setHists(mth2_usys[it->first]);
      }
    }
}    

  // use binStat structure to store the calibration
  // information about tag weight bins we calibrate
  vector<vector<binstat> > binStat;
  binStat.resize(ntbins);
  for(int i=0; i<ntbins; i++)
    binStat[i].resize(nkbins);

  for (int kbin=0; kbin<npTbins*netabins; kbin++) // kinematic bin
    {
      if(verbose)
	{
	  cout << "------------------------------------------------------------" << endl;
	  cout << "Pt/Eta bin " << kbin << "\t[pT] " << ops[0].pt[kbin] << " GeV\t[eta] " << ops[0].eta[kbin] << endl;
	}

      // Coordinates for unfolding in 1D
      int ipt=kbin, ieta=0;
      if (kbin>=npTbins)
	{
	  ieta = kbin/npTbins;
	  ipt -= ieta*npTbins;
	}

      vector<double> vscale;
      vscale.resize(ntbins,1);

      // -------------------------------
      // Compute SumVkk, test MC closure
      // -------------------------------
      double totmc=0, totdt=0;
      double sumvk = 0, totk = 0;
      for (int k=0; k<ntbins; k++)
	{
	  binstat bin_k = compute_comp(ops, kbin, k, lremove_extra);
	  sumvk += bin_k.v;
	  totk += bin_k.pmc;
	}
      if(verbose)
	cout << "--- sum_bin(Pmc)   = " << totk << "\tsqrt(sum_bin(Vkk)) " << sqrt(sumvk) << endl;

      for (int i=0; i<ntbins; i++)
	{
	  // ---------------------------------------------
	  // Compute cov matrix using the reference sample
	  // from the calibration analysis
	  // ---------------------------------------------
	  int indi = kbin*ntbins+i;
	  binstat bin_i = compute_comp(ops, kbin, i, lremove_extra);
	  cov_stat(indi,indi) = Vii(bin_i, sumvk, vscale[i]);
	  cov(indi,indi) = Vii(bin_i, sumvk, vscale[i]);
	  for (int j=i+1; j<ntbins; j++)
	    {
	      int indj = kbin*ntbins+j;
	      binstat bin_j = compute_comp(ops, kbin, j, lremove_extra);
	      cov_stat(indi,indj) = Vij(bin_i, bin_j, sumvk, vscale[i], vscale[j]);
	      cov(indi,indj) = Vij(bin_i, bin_j, sumvk, vscale[i], vscale[j]);
	      cov(indj,indi) = Vij(bin_i, bin_j, sumvk, vscale[i], vscale[j]);
	    }

	  // -----------------------------------------------
	  // Comput SF and systematic uncertainties
	  // using sample/analysis efficiencies if available
	  // -----------------------------------------------
	  bin_i = compute_comp(ops, kbin, i, lremove_extra);
	  binStat[i][kbin] = bin_i;
	  th2_sf->SetBinContent(ipt+1,ieta+1,i+1,bin_i.sf);
	  th2_sf->SetBinError(ipt+1,ieta+1,i+1,sqrt(cov_stat(indi,indi)));
	  // binStat[i][kbin].sf = bin_i.sf;
	  xtrue_eff.at(kbin).at(i) = bin_i.pmc;
	  if(verbose)
	    cout << " SF " << th2_sf->GetBinContent(ipt+1,ieta+1,i+1) << endl;

	  // ---------------------------
	  // Set Reference, Closure test
	  // ---------------------------
	  double xmc = xtrue_eff[kbin][i];
	  mth2_sys["MCreference"]->SetBinContent(ipt+1, ieta+1, i+1, xmc);
	  totmc+=xmc;
	  totdt+=xmc*bin_i.sf;
	  if (verbose and i==ntbins-1)
	    cout << "--- sum_bin(eff_mc) = " << totmc << "\tsum_bin(sf*eff_mc) = " << totdt << endl;

	  // ---------------
	  // systematic part
	  // ---------------
	  double tot=0;
	  map<string,TH3F*>::iterator it;
	  // systematics error
	  for (it  =  mth2_sys.begin(); it !=  mth2_sys.end(); it++)
	    {
	      double xsyst = syst(ops, kbin, i, it->first);
	      mth2_sys[it->first]->SetBinContent(ipt+1, ieta+1, i+1, xsyst);
	      tot += xsyst*xsyst;
	    }
	  for (it  = mth2_usys.begin(); it != mth2_usys.end(); it++)
	    {
	      double xsyst = syst(ops, kbin, i, it->first, "usys");
	      mth2_usys[it->first]->SetBinContent(ipt+1,ieta+1,i+1,xsyst);
	      tot += xsyst*xsyst;
	    }
	  indi = i+ipt*ntbins+ieta*npTbins*ntbins;

	  mth2_sys["systematics"]->SetBinContent(ipt+1,ieta+1,i+1,sqrt(tot));
	  binStat[i][kbin].syst = sqrt(tot);

	} // for tag weight bins
    } // for kinematic bins

  // ----------------------------------
  // Some tests + SF rescaling (unused)
  // ----------------------------------
  //double fscale[npTbins][netabins];
  for (int kbin=0; kbin<nkbins; kbin++)
    {
      double totmc=0, totdt=0;
      int ipt=kbin, ieta=0;
      if (kbin>=npTbins)
	{
	  ipt-=npTbins; ieta=1;
	}
      if(verbose)
	{
	  cout << "------------------------------------------------------------" << endl;
	  cout << "Pt/Eta bin " << kbin << "\t[pT] " << ops[0].pt[kbin] << " GeV\t[eta] " << ops[0].eta[kbin] << endl;
	}
      for (int i=0; i<ntbins; i++)
	{
	  double sf    = th2_sf->GetBinContent(ipt+1,ieta+1,i+1);
	  double effmc = mth2_sys["MCreference"]->GetBinContent(ipt+1,ieta+1,i+1);
	  totmc += effmc;
	  totdt += sf*effmc;
	  if(verbose)
	    cout << "ipt " << ipt << " ieta " << ieta << " bin " << i << " SF " << sf << " effmc " << effmc << " totdt " << totdt << " totmc " << totmc << endl;
	}

      // closure test
      if(verbose)
	{
	  if (fabs(totmc-1)>0.001 || fabs(totdt-1)>0.001)
	    cout << "Normalization check :\t" << "sum_bin(eff_mc) " << totmc << " sum_bin(sf*eff_mc) " << totdt << endl;
	  cout << "Rescaling SF by\t\tsum_bin(eff_mc) / sum_bin(sf*eff_mc) -------->  " << totmc/totdt << endl;
	}
      totmc=0; totdt=0;
      for (int i=0; i<ntbins; i++)
	{
	  double sf    = th2_sf->GetBinContent(ipt+1,ieta+1,i+1);
	  double effmc = mth2_sys["MCreference"]->GetBinContent(ipt+1,ieta+1,i+1);
	  totmc += effmc;
	  totdt += sf*effmc;
	}
      if(verbose)
	{
	  if (fabs(totmc-1)>0.001 || fabs(totdt-1)>0.001)
	    cout << "Normalization check :\t" << "sum_bin(eff_mc) " << totmc << " sum_bin(sf*eff_mc) " << totdt << endl;
	}
    }

  // ------------
  // Final checks
  // ------------
  if(verbose)
    {
      // Systematic errors with large impact in a kinematic bin
      map<string, double>::iterator ity;
      for (ity  = ops[0].sf_sys[0].begin(); ity != ops[0].sf_sys[0].end(); ity++)// systematic source
	{
	  for (int ipt=0; ipt<mth2_sys[ity->first]->GetNbinsX(); ipt++)
	    {
	      for (int ieta=0; ieta<mth2_sys[ity->first]->GetNbinsY(); ieta++)
		{
		  double tot=0;
		  for (int i=0; i<ntbins; i++)
		    {
		      tot += mth2_sys[ity->first]->GetBinContent(ipt+1,ieta+1,i+1)*mth2_sys["MCreference"]->GetBinContent(ipt+1,ieta+1,i+1);
		    }
		  if (fabs(tot)>1e-3)
		    cout << " -> Norm test (syst) " << mth2_sys[ity->first]->GetName() << " " << tot << endl;
		}
	    }
	}

      // Statistical errors with large impact in a kinematic bin
      // formula from c or light ??
      for (ity  = ops[0].sf_sys[0].begin(); ity != ops[0].sf_sys[0].end(); ity++)
	{
	  for(int kbin=0; kbin<nkbins; kbin++)
	    {
	      int ipt=kbin, ieta=0;
	      if (kbin>=npTbins)
		{
		  ieta = kbin/npTbins;
		  ipt -= npTbins;
		}
	      for(int i=0; i<ntbins; i++)
		{
		  int indi = kbin*ntbins+i;
		  double tot = 0;
		  for(int j=0; j<ntbins; j++)
		    {
		      int indj = kbin*ntbins+j;
		      tot+= cov_stat(indi,indj)*mth2_sys["MCreference"]->GetBinContent(ipt+1,ieta+1,j+1);
		    }
		  if (fabs(tot)>1e-3)
		    cout << " -> Norm test (stat) " << ipt << " " << ieta << " " << i << " " << tot << endl;
		}
	    }
	}
    }

  if(verbose_final)
    {
      cout << "Statistical errors [pT/eta;tagbin]" << endl;
      for(int kbin=0; kbin < nkbins; kbin++)
	{
	  for (int i=0; i<ntbins; i++)
	    {
	      int indi = kbin*ntbins+i;
	      cout << sqrt(cov_stat(indi,indi)) << "  ";
	    }
	  cout << endl;
	}

      cout << "SF [pT/eta;tagbin]" << endl;
      for (int ipt=0; ipt<th2_sf->GetNbinsX(); ipt++)
	{
	  for (int ieta=0; ieta<th2_sf->GetNbinsY(); ieta++)
	    {
	      for (int i=0; i<ntbins; i++)
		{
		  cout << th2_sf->GetBinContent(ipt+1,ieta+1,i+1) << "  ";
		}
	      cout << endl;
	    }
	}
    }

  // -------------------------------------
  // systematics eigenvector decomposition
  // is handled in the CDI classes when
  // accessing calibration information
  // -------------------------------------

  // -------------
  // Write to file
  // -------------
  std::string fname = "out_";
  fname += flavour + (string)".root";
  TFile fout(fname.c_str(),"UPDATE");
  gDirectory->mkdir(config.Data());
  fout.cd(config.Data());

  map<string, double>::iterator itx;
  th2_sf->Write("SF");
  cout << "List of systematics :" << endl;
  for (map<string,TH3F*>::iterator it = mth2_sys.begin(); it != mth2_sys.end(); it++)
  {
    cout << it->first << endl;
    mth2_sys[it->first]->Write();
  }
  for (map<string,TH3F*>::iterator it = mth2_usys.begin(); it != mth2_usys.end(); it++)
  {
    cout << it->first << endl;
    mth2_usys[it->first]->Write();
  }
  cov_stat.Write("cov_stat");
  
  TNamed configuration("configuration",config.Data());
  configuration.Write("configuration");
  cout << "---> calibration configuration : " << configuration.GetTitle() << endl;
  fout.Close();

  // ---------------
  // Draw some plots
  // ---------------
  if(drawPlots) {
    gStyle->SetOptStat(0);
    gStyle->SetLabelSize (0.045, "X");
    gStyle->SetLabelSize (0.03, "Y");
    gStyle->SetLegendBorderSize(0);
    gStyle->SetHistLineWidth(3);
    gStyle->SetHistLineColor(kBlack);
    gStyle->SetPadGridY(true);

    sfilePath << "figures/"
	      << flavour << "/"
	      << (jetcoll.find("EM") != string::npos ? "EM" : "LC" )
	      << "/" << tagger << "/";
    gSystem->mkdir(sfilePath.str().c_str(),true);

    TCanvas *c = new TCanvas;
    TLegend *leg = 0;

    // SF plots in fixed tag weight bin
    if(netabins <2) {
      for(int t=0; t<ntbins; t++) {
	stringstream ss;
	ss << "SF [" << tagweight[t] << ", " << tagweight[t+1] << "] vs pt";

	TH1F sfvspt("sfvspt", ss.str().c_str(), npTbins, 0, npTbins);
	TH1F sfvsptb("sfvsptb", ss.str().c_str(), npTbins, 0, npTbins);
	for(int k=0; k<nkbins; k++) {
	  int ipt=k, ieta=0;
	  if (k>=npTbins) {
	    ieta = k/npTbins;
	    ipt -= ieta*npTbins;
	  }
	  double stat = th2_sf->GetBinError(ipt+1, ieta+1, t+1);
	  double syst = mth2_sys["systematics"]->GetBinContent(ipt+1,ieta+1,t+1);
	  sfvspt.SetBinContent(k+1,th2_sf->GetBinContent(ipt+1, ieta+1, t+1));
	  sfvsptb.SetBinContent(k+1,th2_sf->GetBinContent(ipt+1, ieta+1, t+1));
	  sfvspt.SetBinError(k+1,stat);
	  sfvsptb.SetBinError(k+1,stat+syst);
	  sfvspt.GetXaxis()->SetBinLabel(ipt+1, ops[0].pt[ipt].c_str());
	  sfvsptb.GetXaxis()->SetBinLabel(ipt+1, ops[0].pt[ipt].c_str());
	  // set the style
	  sfvsptb.SetFillColorAlpha(kSpring+10,alpha);
	  sfvsptb.SetMarkerStyle(20);
	}

	stringstream shistTitle;
	shistTitle << sfilePath.str() << "sf_vs_pt_tagweightbin_" << t;
	string histTitle = shistTitle.str();
	sfvsptb.Draw("E2");
	sfvspt.Draw("SAME");
	leg = new TLegend(0.15,0.75,0.3,0.85);
	leg->AddEntry(&sfvsptb, "Stat+Syst", "f"); // box
	leg->AddEntry(&sfvspt, "Stat", "lpe"); // error bars
	leg->Draw();
	printCanvas(c, histTitle);
	c->Clear();
	leg->Clear();
      }
    }


    // Scale Factors and efficiencies per tag weight bin
    map<string,TH1F*> mth1_sf;
    map<string,TH1F*> mth1_sfb; // stat+systematic error bands
    TH2F sf("sf", "SF", ntbins, &tagweight[0], nkbins,0,nkbins);
    TH2F sfe("sfe", "SF Error", ntbins, &tagweight[0], nkbins,0,nkbins);
    sf.GetXaxis()->SetTitle("Tagweight");
    sfe.GetXaxis()->SetTitle("Tagweight");

    // plots in fixed kinematic bins
    for (int kbin=0; kbin<nkbins; kbin++) {
      // set coordinates
      int ipt=kbin, ieta=0;
      if (kbin>=npTbins) {
	ieta = kbin/npTbins;
	ipt -= ieta*npTbins;
      }

      c->SetLogz();
      c->SetLogy();

      // set th1 histos
      stringstream shistTitle;
      shistTitle << etabins[ieta] << "<eta<" << etabins[ieta+1] << ":" << pTbins[ipt] << "<pT<" << pTbins[ipt+1];
      string histTitle = shistTitle.str();

      // Data, MC ref and MC sample efficiencies
      TH1F effdata("effdata", histTitle.c_str(),   ntbins, 0, ntbins);
      TH1F effmcref("effmcref", histTitle.c_str(), ntbins, 0, ntbins);
      TH1F effmcsam("effmcsam", histTitle.c_str(), ntbins, 0, ntbins);
      effdata.GetYaxis()->SetRangeUser(1.e-5,1.2);
      effmcref.GetYaxis()->SetRangeUser(1.e-5,1.2);
      effmcsam.GetYaxis()->SetRangeUser(1.e-5,1.2);
      effdata.SetLineColor(kBlack);
      effmcref.SetLineColor(kAzure+1);
      effmcsam.SetLineColor(kPink-3);
      for(int t=0; t<ntbins; t++) {
	effdata.SetBinContent(t+1,binStat[t][kbin].pdt);
	effmcref.SetBinContent(t+1,binStat[t][kbin].pdt/binStat[t][kbin].sf);
	stringstream sbinname;
	sbinname << (string)"[" << tagweight[t] << (string)"; " << tagweight[t+1] << (string)"]";
	effdata.GetXaxis()->SetBinLabel(t+1, sbinname.str().c_str());
	effmcref.GetXaxis()->SetBinLabel(t+1, sbinname.str().c_str());
      }

      leg = new TLegend(0.15,0.15,0.4,0.25);
      leg->AddEntry(&effdata, "Efficiency data", "f");
      leg->AddEntry(&effmcref, "Efficiency MC ref", "f");

      effdata.Draw();
      effmcref.Draw("SAME");

      leg->Draw();
      stringstream sfileTitle;
      sfileTitle << sfilePath.str();
      sfileTitle << "eff_" << etabins[ieta] << "_eta_" << etabins[ieta+1] << "_";
      if(pTbins[ipt] < 100.)
	sfileTitle << "0";
      sfileTitle << pTbins[ipt] << "_pT_" << pTbins[ipt+1];
      printCanvas(c, sfileTitle.str());
      c->Clear();
      leg->Clear();

      c->SetLogy(0);

      // statistical errors
      mth1_sf[histTitle] = new TH1F(histTitle.c_str(),histTitle.c_str(),ntbins,0,ntbins);
      mth1_sf[histTitle]->GetXaxis()->SetTitle("Tagweight");
      mth1_sf[histTitle]->GetYaxis()->SetTitle("SF");
      mth1_sf[histTitle]->GetYaxis()->SetRangeUser(0.,2.5);

      // systematics+statistical errors
      mth1_sfb[histTitle] = new TH1F(histTitle.c_str(),histTitle.c_str(),ntbins,0,ntbins);
      mth1_sfb[histTitle]->GetXaxis()->SetTitle("Tagweight");
      mth1_sfb[histTitle]->GetYaxis()->SetTitle("SF");
      mth1_sfb[histTitle]->GetYaxis()->SetRangeUser(0.,2.5);
      mth1_sfb[histTitle]->SetFillColorAlpha(kSpring+10,alpha);
      mth1_sfb[histTitle]->SetMarkerStyle(20);

      // set th2 histos
      stringstream sbinTitle;
      sbinTitle << "#splitline{" << etabins[ieta] << "<|#eta|<" << etabins[ieta+1] << "}{" << pTbins[ipt] << "<pT<" << pTbins[ipt+1] << "}";
      string binTitle = sbinTitle.str();
      sf.GetYaxis()->SetBinLabel(kbin+1, binTitle.c_str());
      sfe.GetYaxis()->SetBinLabel(kbin+1, binTitle.c_str());
    }

    for (int ipt=0; ipt<th2_sf->GetNbinsX(); ipt++) {
      for (int ieta=0; ieta<th2_sf->GetNbinsY(); ieta++) {

	stringstream shistTitle, sfileTitle;
	shistTitle << etabins[ieta] << "<eta<" << etabins[ieta+1] << ":" << pTbins[ipt] << "<pT<" << pTbins[ipt+1];

	// find a way to recursively create the path
	sfileTitle << sfilePath.str();
	sfileTitle << etabins[ieta] << "_eta_" << etabins[ieta+1] << "_";
	if(pTbins[ipt] < 100.)
	  sfileTitle << "0";
	sfileTitle << pTbins[ipt] << "_pT_" << pTbins[ipt+1];
	string histTitle = shistTitle.str();
	string fileTitle = sfileTitle.str();

	for (int i=0; i<ntbins; i++) {
	  // fill th1f
	  // default = statistical errors
	  stringstream sbinname;
	  sbinname << "[" << tagweight[i] << "; " << tagweight[i+1] << "]";
	  double stat = th2_sf->GetBinError(ipt+1,ieta+1,i+1);
	  mth1_sf[histTitle]->SetBinContent(i+1,th2_sf->GetBinContent(ipt+1,ieta+1,i+1));
	  mth1_sf[histTitle]->SetBinError(i+1,stat);
	  mth1_sf[histTitle]->GetXaxis()->SetBinLabel(i+1,sbinname.str().c_str());

	  // systematic+statistical errors
	  double syst = mth2_sys["systematics"]->GetBinContent(ipt+1,ieta+1,i+1);
	  mth1_sfb[histTitle]->SetBinContent(i+1,th2_sf->GetBinContent(ipt+1,ieta+1,i+1));
	  mth1_sfb[histTitle]->SetBinError(i+1,stat+syst);
	  mth1_sfb[histTitle]->GetXaxis()->SetBinLabel(i+1,sbinname.str().c_str());

	  // fill th2f --> difference between fill(weight) and setbincontent ?
	  sf.SetBinContent(i+1,ieta*npTbins+ipt+1,th2_sf->GetBinContent(ipt+1,ieta+1,i+1));
	  sf.GetXaxis()->SetBinLabel(i+1,sbinname.str().c_str());
	  sfe.SetBinContent(i+1,ieta*npTbins+ipt+1,th2_sf->GetBinError(ipt+1,ieta+1,i+1));
	  sfe.GetXaxis()->SetBinLabel(i+1,sbinname.str().c_str());
	}

	leg = new TLegend(0.15,0.75,0.3,0.85);
	leg->AddEntry(mth1_sfb[histTitle], "Stat+Syst", "f"); // box
	leg->AddEntry(mth1_sf[histTitle], "Stat", "lpe"); // error bars

	mth1_sfb[histTitle]->Draw("E2");
	mth1_sf[histTitle]->Draw("SAME");
	leg->Draw();

	printCanvas(c, fileTitle);
	c->Clear();
	leg->Clear();
	//mth1_sfb[histTitle]->Write();
	delete  mth1_sf[histTitle];    mth1_sf[histTitle] = NULL;
	delete  mth1_sfb[histTitle];   mth1_sfb[histTitle] = NULL;
      }
    }

    gStyle->SetOptStat(0);

    sf.Draw("colz,text");
    printCanvas(c, "figures/sf");
    sfe.Draw("colz,text");
    printCanvas(c,"figures/sferror");
    cov_stat.Draw("colz");
    printCanvas(c, "figures/cov_stat");

    delete c;
    delete leg;
    c = NULL;
    leg = NULL;
    mth1_sf.clear();
    mth1_sfb.clear();
  }

  // -------------------------------------------
  // Write to CDI files --> myBuidContainerNew.C
  // -------------------------------------------

  // Delete some pointers
  delete th2_sf; th2_sf = NULL;
  
}

/*!
  \brief Print the canvas to image files
  \param c canvas name
  \param filename name of the file to be written
*/
void printCanvas(TCanvas *c, string filename)
{
  // c->Print( ((string)filename+".eps").c_str() );
  c->Print( ((string)filename+".pdf").c_str() );
  // c->Print( ((string)filename+".png").c_str() );
}

/*!
  \brief Covariance matrix computation
  \param Binned tagweight information
  \return Diagonal covariance matrix elements
*/
double Vii(binstat bin_i, double sumVkk, double vscale_i)
{
  double p_i   = bin_i.pdt;
  double pmc_i = bin_i.pmc;
  double v_i   = bin_i.v;
  double ntot  = bin_i.ntot;

  double Vii = ((1-2*p_i)*v_i+pow(p_i,2)*sumVkk)/pow(ntot,2);
  Vii /= (pmc_i*pmc_i);
  Vii *= vscale_i*vscale_i;

  return Vii;
}

/*!
  \brief Covariance matrix computation
  \param Binned tagweight information
  \return Non diagonal covariance matrix elements
*/
double Vij(binstat bin_i, binstat bin_j, double sumVkk, double vscale_i, double vscale_j)
{
  double p_i   = bin_i.pdt;
  double pmc_i = bin_i.pmc;
  double v_i   = bin_i.v;

  double p_j   = bin_j.pdt;
  double pmc_j = bin_j.pmc;
  double v_j   = bin_j.v;

  double ntot  = bin_j.ntot;
  if( verbose and (int) bin_i.ntot != (int)bin_j.ntot) {
    cout << "DETECTED UNCONSISTENCY" << endl;
    cout << "bin i " << bin_i.ntot << "bin j " << bin_j.ntot << endl;
  }

  double Vij = (-p_j*v_i - p_i*v_j + p_i*p_j*sumVkk)/pow(ntot,2);
  Vij /= (pmc_i*pmc_j);
  Vij *= vscale_i*vscale_j;

  return Vij;
}

/*!
  \brief Parsing function
  \param sbins Information as a std::string
  \return Numerical values
*/
vector<double> getBins(vector<string> sbins)
{
  vector<double> bins;
  for(int i=0; i<(int)sbins.size(); i++) {
    string ss = sbins[i];
    int i1 = ss.find("<");
    int i2 = ss.find("<", i1+1);

    double low =atof(ss.substr(0,i1).c_str());
    double hig =atof(ss.substr(i2+1).c_str());

    bins.push_back(low);
    bins.push_back(hig);
  }

  sort(bins.begin(), bins.end());
  bins.erase( unique( bins.begin(), bins.end() ), bins.end() );

  return bins;
}

/*!
  \brief Setup histograms
  \param h TH3F histogram
*/
void setHists(TH3F *h)
{
  h->GetXaxis()->SetTitle("pt");
  h->GetYaxis()->SetTitle("abseta");
  h->GetZaxis()->SetTitle("tagweight");
}

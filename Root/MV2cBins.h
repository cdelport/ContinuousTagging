#ifndef MV2cBins_H
#define MV2cBins_H

using namespace std;

map<string,map<int,double> > effmapTopo;
map<string,map<int,double> > effmapTrack;

void setMV2cBins()
{
  // topo jets
  map<int, double> MV2c20_topo;
  MV2c20_topo.emplace(-99, -1.);
  
  
  MV2c20_topo[100] = -1.;
  MV2c20_topo[90] = -0.9185;
  MV2c20_topo[85] = -0.7887;
  MV2c20_topo[80] = -0.5911;
  MV2c20_topo[77] = -0.4434;
  MV2c20_topo[70] = -0.0436;
  MV2c20_topo[60] = 0.4496;
  MV2c20_topo[50] = 0.7535;
  MV2c20_topo[30] = 0.9540;
  
  map<int, double> MV2c10_topo;
  MV2c10_topo[100] = -1.;
  MV2c10_topo[85] = 0.1758475;
  MV2c10_topo[77] = 0.645925;
  MV2c10_topo[70] = 0.8244273;
  MV2c10_topo[60] = 0.934906;
  MV2c10_topo[50] = 0.9769329;
  MV2c10_topo[30] = 0.9977155;
  
  effmapTopo["MV2c20"] = MV2c20_topo;
  effmapTopo["MV2c10"] = MV2c10_topo;
  
  // track jets
  map<int, double> MV2c10_trk;
  MV2c10_trk[100] = -1.;
  MV2c10_trk[85] = -0.1416;
  MV2c10_trk[77] = 0.3706;
  MV2c10_trk[70] = 0.6455;
  MV2c10_trk[60] = 0.8529;
  MV2c10_trk[50] = 0.9452;
  MV2c10_trk[30] = 0.9951;
  
  effmapTrack["MV2c10"] = MV2c10_trk;
  
}

#endif

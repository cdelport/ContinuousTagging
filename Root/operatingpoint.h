/*!
   \file operatingpoint.h
   \brief Structure for Operation Point information
*/

#ifndef OPERATINGPOINT_H
#define OPERATINGPOINT_H

#include <vector>
#include <map>

using namespace std;

/** 
\struct op
\brief Cumulative calibration result of a given WP
\note Information for every working/operating points as defined from cumulative distributions
*/
struct op{

  vector<string> pt;                       /*!< pT bin */
  vector<string> eta;                      /*!< eta bin */
  vector<double> ntag;                     /*!< Number of tags */
  vector<double> n;                        /*!< Number of jets for the inclusive WP */
  vector<double> effMC;                    /*!< Monte-Carlo tagging efficiency for the */
  vector<double> sig_ntag;                 /*!< Number of tags uncertainty */
  vector<double> sig_n;                    /*!< Number of jets uncertainty */
  vector<double> sf;                       /*!< Cumulative Scale Factor */
  vector< map<string,double> > sf_sys;     /*!< Systematic uncertainties on the cumulative Scale Factor */
  vector< map<string,double> > sf_usys;    /*!< Systematic uncertainties on the cumulative Scale Factor */
  vector<double> sf_stat;                  /*!< Cumulative Scale Factor uncertainty */
  vector<double> extfact;
  vector<double> extratio;
};

#endif

/*!
   \file computecomp.h
   \brief Compute the tagweight binned information from inclusive WP
*/

#ifndef COMPUTECOMP_H
#define COMPUTECOMP_H

#include <vector>
#include <math.h>

#include "operatingpoint.h"
#include "binstat.h"

/*!
Compute the statistical information on a tag bin
from the cumulative working points and return it
as a binstat object as defined in binstat.h
*/

using namespace std;

/*!
   \brief Compute the tagweight binned information from inclusive WP
   \param ops Inclusive WP calibration results
   \param kbin Kinematic bin index
   \param i Tagweight bin index
   \pre Read the inclusive calibration results
   \return binstat object with the pseudo-continuous calibration of one tagweight bin
*/
binstat compute_comp(vector<op> ops, int kbin, int i, bool lremove_extra = false){
  
  double pdt, pmc, sf, v, ntot;
  double epsmc_i,   epsdata_i,   sf_i;
  double epsmc_ip1, epsdata_ip1, sf_ip1;
  
  if (lremove_extra) {
    for (int j=0; j<(int)ops.size(); j++) {
      for (int k=0; k<(int)ops[j].sf.size(); k++) {
        ops[j].sf[k] /= ops[j].extfact[k];
      }
    }
  }

  if (i==0) 
  {
    epsmc_i     = 1;
    epsdata_i   = 1;
    sf_i        = 1;
    epsdata_ip1 = ops[i].ntag[kbin]/ops[i].n[kbin];
    epsmc_ip1   = ops[i].effMC[kbin];
    if(epsmc_ip1 == 0.)
    epsmc_ip1   = epsdata_ip1/ops[i].sf[kbin];
    sf_ip1      = ops[i].sf[kbin];
    v           = pow(ops[i].sig_n[kbin],2);// at eff = 1, sig_ntag = sig_n
    v          -= pow(ops[i].sig_ntag[kbin],2);
    ntot        = ops[i].n[kbin];
  } 
  else if (i==(int)ops.size()) 
  {
    epsdata_i   = ops[i-1].ntag[kbin]/ops[i-1].n[kbin];
    epsmc_i = ops[i-1].effMC[kbin];
    if(epsmc_i == 0.)
    epsmc_i     = epsdata_i/ops[i-1].sf[kbin];
    sf_i        = ops[i-1].sf[kbin];
    epsdata_ip1 = 0;
    epsmc_ip1   = 0;
    sf_ip1      = 1;
    v           = pow(ops[i-1].sig_ntag[kbin],2);// eff = 0, sig_ntag = 0
    ntot        = ops[i-1].n[kbin];
  }
  else
  {
    epsdata_i   = ops[i-1].ntag[kbin]/ops[i-1].n[kbin];
    epsmc_i = ops[i-1].effMC[kbin];
    if(epsmc_i == 0.)
    epsmc_i     = epsdata_i/ops[i-1].sf[kbin];
    sf_i        = ops[i-1].sf[kbin];
    epsdata_ip1 = ops[i].ntag[kbin]/ops[i].n[kbin];
    epsmc_ip1   = ops[i].effMC[kbin];
    if(epsmc_ip1 == 0.)
    epsmc_ip1   = epsdata_ip1/ops[i].sf[kbin];
    sf_ip1      = ops[i].sf[kbin];
    v           = pow(ops[i-1].sig_ntag[kbin],2)-pow(ops[i].sig_ntag[kbin],2);
    ntot        = ops[i].n[kbin];
  }

  pdt = epsdata_i-epsdata_ip1;
  pmc = epsmc_i-epsmc_ip1;
  sf = (sf_i*epsmc_i-sf_ip1*epsmc_ip1)/(epsmc_i-epsmc_ip1);

  //if(fabs(pdt/pmc-sf)>0.001)
  //cout << "sf not matching " << endl;

  if (lremove_extra) {
    for (int j=0; j<(int)ops.size(); j++) {
      for (int k=0; k<(int)ops[j].sf.size(); k++) {
        ops[j].sf[k] *= ops[j].extfact[k];
        //	ops[j].sf[k] *= ops[j].extratio[k];
        //	cout << ops[j].extfact[k] << endl;
        //	cout << ops[j].extratio[k] << endl;
        //	cout << "fact " << ops[j].extfact[k]*ops[j].extratio[k] << endl;
      }
    }
  }

  binstat bin;
  bin.pdt  = pdt;
  bin.pmc  = pmc;
  bin.sf   = sf;
  bin.v    = v;
  bin.ntot = ntot;
  
  return bin;
}

#endif

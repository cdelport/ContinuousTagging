/*!
   \file syst.h
   \brief Compute systematic uncertainty within a tagweight bin
*/

#ifndef SYST_H
#define SYST_H

#include <vector>
#include "operatingpoint.h"

using namespace  std;

/*!
   \brief Compute systematic uncertainty
   \param ops Cumulative calibration result (op)
   \param kbin pT/eta bin
   \param i tagweight bin
   \param tag systematic name
   \return Uncertainty value on the pseudo-continuous Scale Factor
*/
double syst(vector<op>ops, int kbin, int i, string tag, string ssyst="sys"){
  
  map<string, double>::iterator it;
  
  double epsmc_i,   epsdata_i;
  double epsmc_ip1, epsdata_ip1;
  double S_i, S_ip1;
  
  if (i==0) 
  {
    S_i   = 0;
    if (ssyst=="sys") 
    {
      map<string, double>::iterator it;
      S_ip1 = (ops[i].sf_sys[kbin].find(tag))->second;
    } 
    else 
    {
      S_ip1 = (ops[i].sf_usys[kbin].find(tag))->second;
    }
    epsdata_i    = 1;
    epsmc_i      = 1;
    epsdata_ip1  = ops[i].ntag[kbin]/ops[i].n[kbin];
    epsmc_ip1    = epsdata_ip1/ops[i].sf[kbin];
  } 
  else if (i==(int)ops.size()) 
  {
    S_ip1        = 0;
    if (ssyst=="sys") 
    {
      S_i = (ops[i-1].sf_sys[kbin].find(tag))->second;
    } 
    else 
    {
      S_i = (ops[i-1].sf_usys[kbin].find(tag))->second;
    }
    epsdata_i    = ops[i-1].ntag[kbin]/ops[i-1].n[kbin];
    epsmc_i      = epsdata_i/ops[i-1].sf[kbin];
    epsdata_ip1  = 0;
    epsmc_ip1    = 0;
  } 
  else 
  {
    if (ssyst=="sys") 
    {
      S_i    = (ops[i-1].sf_sys[kbin].find(tag))->second;
      S_ip1  = (ops[i].sf_sys[kbin].find(tag))->second;
    } 
    else 
    {
      S_i    = (ops[i-1].sf_usys[kbin].find(tag))->second;
      S_ip1  = (ops[i].sf_usys[kbin].find(tag))->second;
    }
    epsdata_i    = ops[i-1].ntag[kbin]/ops[i-1].n[kbin];
    epsmc_i      = epsdata_i/ops[i-1].sf[kbin];
    epsdata_ip1  = ops[i].ntag[kbin]/ops[i].n[kbin];
    epsmc_ip1    = epsdata_ip1/ops[i].sf[kbin];
  }
  
  if (fabs((S_i*epsmc_i - S_ip1*epsmc_ip1)/(epsmc_i - epsmc_ip1))>0.2) 
  {
    //  cout << "--> " << tag << endl;
    //  cout << " " << S_i << " " << S_ip1 << endl;
    //  cout << " " << epsmc_i << " " << epsmc_ip1 << endl;
    //  cout << " " << (S_i*epsmc_i - S_ip1*epsmc_ip1)/(epsmc_i - epsmc_ip1) << endl;
  }

  return (S_i*epsmc_i - S_ip1*epsmc_ip1)/(epsmc_i - epsmc_ip1);
}

#endif

/*
root -l 
[]  gSystem -> Load("libCalibrationDataInterface.so");
[] .L buildContinuousCDI.C+
[] buildContinuousCDI()
*/

#include <TROOT.h>
#include <TFile.h>
#include <TSystem.h>
#include <TClass.h>
#include <TNamed.h>
#include <TKey.h>
#include <TString.h>

#include <TVectorT.h>
#include <TMatrixDSym.h>
#include <TH3.h>
#include <TH2.h>
#include <TH1.h>
#include <TF1.h>

#include <iostream>
#include <cmath>
#include <string>
#include <map>
#include <algorithm>
#include <utility>

#include "CalibrationDataVariables.h"
#include "CalibrationDataBackend.h"
#include "CalibrationDataContainer.h"

using namespace std;
using namespace Analysis;

using Analysis::CalibrationDataContainer;
using Analysis::CalibrationDataHistogramContainer;
using Analysis::CalibrationDataFunctionContainer;
using Analysis::CalibrationDataVariables;
using Analysis::UncertaintyResult;

void copyEfficiencyMaps(TString effFileName, bool continuousDirOnly);

string setname(const char* cname){
  string ostr="none";
  string str(cname);
  if (str.find("105860")!=string::npos)      // ttbar - HerwigHimmy
    ostr = "HerwigJimmy";
  else if (str.find("105200")!=string::npos) // ttbar - HerwigJimmy MC@NLO
    ostr = "HerwigJimmy";
  else if (str.find("110198")!=string::npos) // ttbar - Herwig
    ostr = "Herwigpp";
  else if (str.find("110898")!=string::npos) // ttbar - Pythia8
    ostr = "Pythia8";
  else if (str.find("181380")!=string::npos) // ttbar - Sherpa
    ostr = "Sherpa";
  else if (str.find("117050")!=string::npos) // ttbar - PowhegPythia
    ostr = "Pythia6";
  else if (str.find("110140")!=string::npos) // Wt - PowhegPythia
    ostr = "Pythia6";
  else if (str.find("110119")!=string::npos) // single top - PowhegPythia
    ostr = "Pythia6";
  else if (str.find("129270")!=string::npos) // dijets - Pythia8EvtGen
    ostr = "Pythia8EvtGen";
  else if (str.find("147910")!=string::npos) // dijets - Pythia8
    ostr = "Pythia8";
  else if (str.find("187001")!=string::npos) // ttbar  - PowhegPythia8
    ostr = "Pythia8";
  return ostr;
}

void plotCov(TMatrixDSym *h){
  int nrow = h->GetNrows();
  TH2D *hc = new TH2D("hcov","",nrow,0,nrow,nrow,0,nrow);
  hc->SetTitle("");
  for (int i=0;i<nrow;i++){
    for (int j=0;j<nrow;j++){
      double sii = sqrt(fabs((*h)(i,i)));
      double sjj = sqrt(fabs((*h)(j,j)));
      hc->SetBinContent(i,j,(*h)(i,j)/(sii*sjj));
    }
  }
  hc->Draw("COLZ");
  hc->SetTitle("");
  hc->SetMaximum(1);
  hc->SetMinimum(-1);
}

void buildAndCopy(TH2 *hold, TH2*& h){

  int nx = hold->GetNbinsX();
  int ny = hold->GetNbinsY();

  double *xbins = new double[nx+1];
  double *ybins = new double[ny+1];

  for (int ix=1;ix<=nx;ix++)
    xbins[ix-1] = hold->GetXaxis()->GetBinLowEdge(ix)/1000;
  xbins[nx] = hold->GetXaxis()->GetBinUpEdge(nx)/1000;

  for (int iy=1;iy<=ny;iy++)
    ybins[iy-1] = hold->GetYaxis()->GetBinLowEdge(iy);
  ybins[ny] = hold->GetYaxis()->GetBinUpEdge(ny);

  h = new TH2D(hold->GetName(),"",nx,xbins,ny,ybins);

  //  h = (TH2*)hold->Clone();
  //  h->Reset();
  h->GetXaxis()->SetTitle("pt");
  h->GetYaxis()->SetTitle("tagweight");

  for (int ix=1;ix<=nx;ix++){
    for (int iy=1;iy<=ny;iy++){
      h->SetBinContent(ix,iy,hold->GetBinContent(ix,iy));
      h->SetBinError(ix,iy,hold->GetBinError(ix,iy));
      //      if (ix==1 && iy==1)
      //        cout << hold->GetBinContent(ix,iy) << endl;
    }
  }

}

void FillError(TH2* syst){
  for (int i=1;i<=syst->GetNbinsX();i++){
    for (int j=1;j<=syst->GetNbinsY();j++){
      syst->SetBinError(i,j,syst->GetBinContent(i,j));
    }
  }
}

void FillError(TH3* syst){
  for (int i=1;i<=syst->GetNbinsX();i++){
    for (int j=1;j<=syst->GetNbinsY();j++){
      for (int k=1;k<=syst->GetNbinsZ();k++){
	syst->SetBinError(i,j,k,syst->GetBinContent(i,j,k));
      }
    }
  }
}

void AddSyst(TH2* syst, TH2* h){
  double tmp;
  for (int i=1;i<=syst->GetNbinsX();i++){
    for (int j=1;j<=syst->GetNbinsY();j++){
      tmp = syst->GetBinContent(i,j);
      tmp = tmp + h->GetBinContent(i,j)*h->GetBinContent(i,j);
      syst->SetBinContent(i,j,tmp);
    }
  }
}

void SqrtSyst(TH2* syst){
  double tmp;
  for (int i=1;i<=syst->GetNbinsX();i++){
    for (int j=1;j<=syst->GetNbinsY();j++){
      tmp = syst->GetBinContent(i,j);
      syst->SetBinContent(i,j,sqrt(tmp));
    }
  }
}

void AddSyst(TH3* syst, TH3* h){
  double tmp;
  for (int i=1;i<=syst->GetNbinsX();i++){
    for (int j=1;j<=syst->GetNbinsY();j++){
      for (int k=1;k<=syst->GetNbinsZ();k++){
	tmp = syst->GetBinContent(i,j,k);
	tmp = tmp + h->GetBinContent(i,j,k)*h->GetBinContent(i,j,k);
	syst->SetBinContent(i,j,k,tmp);
      }
    }
  }
}

void SqrtSyst(TH3* syst){
  double tmp;
  for (int i=1;i<=syst->GetNbinsX();i++){
    for (int j=1;j<=syst->GetNbinsY();j++){
      for (int k=1;k<=syst->GetNbinsZ();k++){
	tmp = syst->GetBinContent(i,j,k);
	tmp = sqrt(tmp);
	syst->SetBinContent(i,j,k,tmp);
      }
    }
  }
}

void buildContinuousCDI(){

  // You seem to prefer histograms to behave like all other objects when it
  // comes to object lifetime, i.e. you want to delete them instead of TFile
  // deleting them:
  TH1::AddDirectory(kFALSE);

  #ifdef __CINT__
  gSystem->Load("libCalibrationDataInterface.so");
  #endif
  
  cout << "Continuous.root wil be removed ... ok ?" << endl;
  cin.get();

  string tagger, jetcoll;
  
  // -------------------------------------------------------------
  // Open the output file, where the CDI objects are to be written
  // -------------------------------------------------------------
  TString fname = "Continuous.root";
  TFile *f = TFile::Open(fname, "CREATE"); // update if merge to official CDI
  if (f->IsZombie()) {
    std::cout << "error: cannot create file " << fname << std::endl;
    delete f;
    return;
  }

  map<TString, int> tagmap; // filename, flavour
  tagmap["out_charm.root"]  = 4;
  tagmap["out_charm_ttbar.root"]  = 4;
  tagmap["out_tau.root"]    = 15;
  tagmap["out_tau_ttbar.root"]    = 15;
  tagmap["out_light.root"]  = 0;
  tagmap["out_bottom.root"] = 5;
  tagmap["out_bottom_pdf.root"] = 5;
  
  
  for(map<TString,int>::iterator it = tagmap.begin(); it != tagmap.end(); it++) {
    TFile* fin = new TFile(it->first, "READ");
    int flav = it->second;
    TString flavour =
    (flav == 0)  ? "Light" :
    (flav == 4)  ? "C" :
    (flav == 5)  ? "B" :
    (flav == 15) ? "T" : "?";
    
    bool recompute_syst = (flav == 0) ? true : false;
    
    // --------------------------------------------------------------------------
    // Read the directories corresponding to different calibration configurations
    // --------------------------------------------------------------------------
    TKey *key_dir;
    TIter lnext_dir(fin->GetListOfKeys());
    while ((key_dir = (TKey*)lnext_dir())){
      TClass *cl_dir = gROOT->GetClass(key_dir->GetClassName());
      if (cl_dir->InheritsFrom("TDirectory")){
        TDirectory *dir = (TDirectory*)key_dir->ReadObj();
        
        TString binning = TString(dir->GetName())(TString(dir->GetName()).Length()-8,8);
        cout << "Tag weight bin config " << binning << endl;
        TString containerPattern = it->first.Contains("pdf") ? TString("PDF_") : TString("IntegratedWP_");
        containerPattern += binning;
        
        CalibrationDataHistogramContainer* chSF_it  = new CalibrationDataHistogramContainer(containerPattern + "_SF"); 
        CalibrationDataHistogramContainer* chEff_it = new CalibrationDataHistogramContainer(containerPattern + "_Eff");
        
        // ------------------------------------------------
        // Read the objects stored in the current directory
        // ------------------------------------------------
        TKey *key;
        bool lfirst=true;
        TH3 *systl;
        TIter lnext(dir->GetListOfKeys());
        while ((key = (TKey*)lnext())) {
          TClass *cl = gROOT->GetClass(key->GetClassName());
          if (cl->InheritsFrom("TH3")){
            TH3 *h = (TH3*)key->ReadObj();
            
            if (recompute_syst){
              if (lfirst){
                systl  = (TH3*)h->Clone();
                systl->Reset();
                lfirst = false;
              }
            }
            
            // ----------------------------------------------
            // Identify the object and feed the CDI container
            // ----------------------------------------------
            TString name(h->GetName());
            if (recompute_syst)
            // for charms and tau, set "b purity low pT" to uncorrelated" ? low pT only ?
            if (name!="SF" && name!="MCreference" && name!="SFx" && name!="MCreferencex")
            AddSyst(systl,h);
            if (name!="SF" && name!="MCreference" && name!="SFx" && name!="MCreferencex")
            FillError(h);
            if (name=="SF")
            chSF_it->setResult(h);
            else if (name=="MCreference"){ // for b, MCreference = Eff
              chEff_it->setResult(h);
              chSF_it->setUncertainty(name.Data(),h);
            } else if (name=="MC statistics" || name=="simulation tagging efficiency"){
              cout << "Uncorr " << name << endl;
              chSF_it->setUncertainty(name.Data(),h);
              chSF_it->setUncorrelated(name.Data());
            } else 
            chSF_it->setUncertainty(name.Data(),h);	
          } else {
            TMatrixDSym *hs = (TMatrixDSym*)key->ReadObj();
            if(flav ==0)
            chSF_it->setUncertainty("statistics",hs);
          }
        }
        
        if (recompute_syst) {
          SqrtSyst(systl);
        }
        
        chSF_it->restrictToRange(true);
        chSF_it->restrictToRange(true);
        
        // -----------------------------------
        // Retrieve the calibration parameters
        // -----------------------------------
        TString config, hadronisation = "Pythia8EvtGen"; //"Pythia6EvtGen";
        TNamed  *configuration = (TNamed*)dir->Get("configuration");
        if(configuration != NULL){
          config = configuration->GetTitle();
          //hadronisation = config.Contains("Pythia6") ? "Pythia6" : (config.Contains("Pythia8") ? "Pythia8" : "hadr");
          std::cout << "!!! Hadronisation forced to be " << hadronisation << std::endl;
          cin.get();
          tagger  = config.Contains("MV1c") ? "MV1c" : (config.Contains("MV1") ? "MV1" : (config.Contains("MV2c20") ? "MV2c20" : (config.Contains("MV2c10") ? "MV2c10" : "XXX") ) );
          jetcoll = config.Contains("EM")   ? "EM"   : (config.Contains("LC")  ? "LC"  : "YY") ;
        }
        else {
          cout << "No configuration data retrieved" << endl;
          return;
        }
        string strjet = "AntiKt4Topo";
        strjet += jetcoll;
        strjet += "JVF0_5";
        
        // -----------------------
        // Write the CDI container
        // -----------------------
        cout << "Writing CDI object in " << tagger << "/" << "AntiKt4EMTopoJets" << "/" << "Continuous" << "/" << flavour << endl;
        chSF_it->setHadronisation(hadronisation.Data());
        chEff_it->setHadronisation(hadronisation.Data());
        
        if(containerPattern.Contains("PDF") and (flav == 5) and !containerPattern.Contains("7TagBins"))
        {
          addContainer(chSF_it, f,tagger.c_str(), "AntiKt4EMTopoJets", "Continuous", flavour.Data(), "default_SF");
          addContainer(chSF_it, f, tagger.c_str(), "AntiKt4EMTopoJets", "Continuous", flavour.Data(), containerPattern + "_SF");
        }
        else if ((flav == 4) or (flav == 15))
        {
          if ( it->first.Contains("_ttbar") )
          {
            addContainer(chSF_it, f,tagger.c_str(), "AntiKt4EMTopoJets", "Continuous", flavour.Data(), "default_SF");
            addContainer(chSF_it, f,tagger.c_str(), "AntiKt4EMTopoJets", "Continuous", flavour.Data(), "ttbarC_SF");
          } else {
            addContainer(chSF_it, f,tagger.c_str(), "AntiKt4EMTopoJets", "Continuous", flavour.Data(), "Wc_SF");
          }
        }
        else if(!containerPattern.Contains("PDF") and (flav != 5) and (flav != 4) and (flav != 15))
        {
          addContainer(chSF_it, f,tagger.c_str(), "AntiKt4EMTopoJets", "Continuous", flavour.Data(), "negativetags_SF");
          addContainer(chSF_it, f,tagger.c_str(), "AntiKt4EMTopoJets", "Continuous", flavour.Data(), "default_SF");
        }
	
        // delete some pointers
        delete chSF_it;  chSF_it  = NULL;
        delete chEff_it; chEff_it = NULL;
        delete configuration; configuration = NULL;
        
      } // subdirectory structure
    } // subdirectory structure
    
    delete fin; fin = NULL;
  } // tagmap
  
  f->Close();
  delete f; f = NULL;

  cout << "--> copy efficency maps" << endl;
  copyEfficiencyMaps("2016-20_7-13TeV-MC15-CDI-2017-04-05_v1.root", true);
  
}


void copyEfficiencyMaps(TString effFileName, bool continuousDirOnly)
{
  TFile *efficiencyMapFile = new TFile(effFileName, "READ");

  if (efficiencyMapFile->IsZombie())
  {
    cout << "File containing efficiency maps could not be opened --> " << effFileName << endl;
    cin.get();
  }
  
  TKey *key_tagdir;
  TIter lnext_tagdir(efficiencyMapFile->GetListOfKeys());
  while ((key_tagdir = (TKey*)lnext_tagdir())){
    TClass *cl_tagdir = gROOT->GetClass(key_tagdir->GetClassName());
    if (cl_tagdir->InheritsFrom("TDirectory")){
      TDirectory *tagdir = (TDirectory*)key_tagdir->ReadObj();
      TString tagger = key_tagdir->GetName();
      cout << "- tagger " << tagger << endl;
      
      TKey *key_coldir;
      TIter lnext_coldir(tagdir->GetListOfKeys());
      while ((key_coldir = (TKey*)lnext_coldir())){
        TClass *cl_coldir = gROOT->GetClass(key_coldir->GetClassName());
        if (cl_coldir->InheritsFrom("TDirectory")){
          TDirectory *coldir = (TDirectory*)key_coldir->ReadObj();
          TString collection = key_coldir->GetName(); // coldir->GetTitle();
          cout << "-- collection " << collection << endl;
          
          TKey *key_contdir;
          TIter lnext_contdir(coldir->GetListOfKeys());
          while ((key_contdir = (TKey*)lnext_contdir())){
            TClass *cl_contdir = gROOT->GetClass(key_contdir->GetClassName());
            if (cl_contdir->InheritsFrom("TDirectory")){
              TDirectory *contdir = (TDirectory*)key_contdir->ReadObj();
              TString continuous = key_contdir->GetName(); // contdir->GetTitle();
              cout << "--- calibration " << continuous << endl;
              
              if (continuousDirOnly and !continuous.Contains("Continuous")) continue;
              
              TKey *key_flavdir;
              TIter lnext_flavdir(contdir->GetListOfKeys());
              while ((key_flavdir = (TKey*)lnext_flavdir())){
                TClass *cl_flavdir = gROOT->GetClass(key_flavdir->GetClassName());
                
                if (cl_flavdir->InheritsFrom("TDirectory")){
                  TDirectory *flavdir = (TDirectory*)key_flavdir->ReadObj();
                  TString flavour = key_flavdir->GetName(); // flavdir->GetTitle();
                  cout << "---- flavour " << flavour << endl;
                  
                  TKey *key_container;
                  TIter lnext_container(flavdir->GetListOfKeys());
                  while ((key_container = (TKey*)lnext_container())){
                    TClass *cl_container = gROOT->GetClass(key_container->GetClassName());
                    if (cl_container->InheritsFrom("TDirectory")){
                      TDirectory *directory = (TDirectory*)key_container->ReadObj();
                      cout << "directory " << directory->GetTitle() << endl;
                    }
                    if (!(cl_container->InheritsFrom("TH3D") or cl_container->InheritsFrom("TH2D") or cl_container->InheritsFrom("TH1D") or cl_container->InheritsFrom("TString")  )){
                      
                      CalibrationDataHistogramContainer* chEff = (CalibrationDataHistogramContainer*)(key_container->ReadObj()->Clone());
                      
                      TFile *file = new TFile ("Continuous.root","UPDATE");
                      
                      if (file->GetDirectory(tagger) == NULL) file->mkdir(tagger);
                      if (file->GetDirectory(tagger + "/" + collection) == NULL) file->mkdir(tagger + "/" + collection);
                      if (file->GetDirectory(tagger + "/" + collection + "/" + continuous) == NULL) file->mkdir(tagger + "/" + collection + "/" + continuous);
                      if (file->GetDirectory(tagger + "/" + collection + "/" + continuous + "/" + flavour) == NULL) file->mkdir(tagger + "/" + collection + "/" + continuous + "/" + flavour);
                      
                      addContainer(chEff, file, tagger, collection, continuous, flavour, key_container->GetName());
                      
                      TString path = tagger + "/" + collection + "/" + continuous + "/" + flavour + "/";
                      cout << "---->Copying efficiency map to " << path << "/" << key_container->GetName() << endl;
                      
                      file->Close();
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  
  efficiencyMapFile->Close();
  delete efficiencyMapFile;
}
